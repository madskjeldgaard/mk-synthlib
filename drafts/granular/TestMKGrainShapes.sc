TestMKGrainShapes : UnitTest {
  setUp{
      MKLoad.loadAll();
      // MKGrainShapes.new();
  }

  tearDown {
    MKLoad.removeAll();
  }

  test_name{
    var c = MKGrainShapes.new;
    this.assert(
      c.name.isNil.not,
      "component has name"
    );

    this.assert(
      MKLoad.exists(c.name),
      "name corresponds to loaded component"
    );
  }

  test_grain_buffers{
    fork{
      var component;

      this.bootServer;
      component= MKGrainShapes.new;
      component.buffers.keysValuesDo{|key, item|
        this.assert(item.class == Buffer);
      }
    }
    
  }

  test_grain_envelopes{
    fork{
      var component;

      this.bootServer;

      component = MKGrainShapes.new;
      component.items.items.keysValuesDo{|key, item|
        this.assert(item.class == Env, "function wrapper % is a envelope".format(key));
      }
    }
  }
}
