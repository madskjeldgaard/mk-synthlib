(
M.add("zosc", {|dur, freqOffset=0|
    var lagTime = \lagTime.kr(1);
	ZOsc.ar(
		freq: (MKNC.kr("freq", 100) + freqOffset).mklag(lagTime),
		formantfreq: MKNC.kr("formantfreq", 91, spec: Spec.specs[\freq]).mklag(lagTime) + freqOffset,
		shape: MKNC.kr("shape", 0.5).mklag(lagTime),
		mode: MKNC.kr("mode", spec: [-1.0,1.0]).mklag(lagTime)
	)
}, numChannelsIn: 1);
)
