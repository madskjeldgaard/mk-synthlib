(
M.add("varshape", {|dur, freqOffset=0|
    var lagTime = \lagTime.kr(1);
	VarShapeOsc.ar(
		freq: (MKNC.kr("freq", 100) + freqOffset).mklag(lagTime),
		pw: MKNC.kr("pw", 0.5).mklag(lagTime),
		waveshape: MKNC.kr("shape", 0.5).mklag(lagTime),
		sync: MKNC.kr("sync", 1).mklag(lagTime),
		syncfreq: MKNC.kr("syncfreq", 101, spec: Spec.specs[\freq]).mklag(lagTime)
	);
}, numChannelsIn: 1);
)
