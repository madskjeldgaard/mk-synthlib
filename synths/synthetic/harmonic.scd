(
M.add("harmonic", {|dur, freqOffset=0|
    var lagTime = \lagTime.kr(1);

	var amplitudes = Array.fill(16, {|i| MKNC.kr("amp%".format(i+1), 1/16) });
	HarmonicOsc.ar(freq: (MKNC.kr("freq", 100) + freqOffset).mklag(lagTime),  firstharmonic: MKNC.kr("first", 3),  amplitudes: amplitudes)
}, numChannelsIn: 1);
)
