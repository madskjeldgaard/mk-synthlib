(
M.add("vosim", {|dur, freqOffset=0|
    var lagTime = \lagTime.kr(1);
	VosimOsc.ar(
		freq: (MKNC.kr("freq", 100) + freqOffset).mklag(lagTime),
		form1freq: MKNC.kr("form1freq", 1054, spec: Spec.specs[\freq]).mklag(lagTime),
		form2freq: MKNC.kr("form2freq", 454, spec: Spec.specs[\freq]).mklag(lagTime),
		shape: MKNC.kr("shape", 0, spec: [-1.0,1.0]).mklag(lagTime)
	);

	}, numChannelsIn: 1);
)
